import smbus
import RPi.GPIO as GPIO
import time as time
import scratch

i2c = smbus.SMBus(1)
address = 0x4d

scratchConnect = False
InventemADCConnect = False

adcValue = 0

while True: 

        if not scratchConnect:
                try:
                        s = scratch.Scratch()
                        s.disconnect()
                        scratchConnect = True
                except:
                        print "Could not connect to Scratch"
                        print "Wait 1s and try again"
                        time.sleep(1)
        else:
                try:
                        adcHigh, adcLow = i2c.read_i2c_block_data(address, 0, 2)
                        
                        InventemADCConnect = True
                except:    
                        print "Could not connect to ADC Bridge"
                        print "Wait 1s and try again"
                        InventemADCConnect = False
                        time.sleep(1)

        if InventemADCConnect and scratchConnect:

                adcValue = (adcHigh << 8) + adcLow
                adcValue = adcValue >> 2
      
                try:
                       s = scratch.Scratch()
                       s.sensorupdate({'adcInput' : adcValue})
                       s.disconnect()
 
                except:
                        scratchConnect = False
                        s.disconnect()
                        print "Lost scratch connection"

                time.sleep(0.05)
